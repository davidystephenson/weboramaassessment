import React from 'react'

export default class Beer extends React.Component {
  constructor () {
    super()

    this.favorite = this.favorite.bind(this)
  }

  favorite () {
    this.props.favorite(this.props.beer)
  }

  render () {
    return <div>
      {this.props.beer.name} ({this.props.beer.id}) <button onClick={this.favorite}>Favorite</button>
    </div>
  }
}
