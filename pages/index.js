import React from 'react'
import Beer from './Beer'

const fetch = require('isomorphic-fetch')
const get = url => fetch(url)
  .then(response => response.ok ? response.json() : null)

const getBeerLists = async number => Promise
  .all(new Array(number)
    .fill('https://api.punkapi.com/v2/beers/random')
    .map(get)
  )

const stockBar = async number => {
  const beerLists = await getBeerLists(number)

  return beerLists.map(list => list[0])
}

export default class Index extends React.Component {
  static async getInitialProps () {
    const beers = await stockBar(10)

    return { beers }
  }

  constructor (props) {
    super(props)

    this.state = { favorites: [], beers: props.beers }

    this.setBeers = this.setBeers.bind(this)
    this.favorite = this.favorite.bind(this)
  }

  favorite (beer) {
    const favorites = this.state.favorites.concat(beer)

    this.setState({ favorites })
  }

  async setBeers (event) {
    const button = event.target
    button.textContent = 'Loading...'
    button.disabled = true

    const beers = await stockBar(10)

    button.textContent = 'Get'
    button.disabled = false

    this.setState({ beers })
  }

  render () {
    const brew = (beer, index) => <Beer key={index} beer={beer} favorite={this.favorite} />

    return <div>
      <h2>Beers</h2>
      { this.state.beers.map(brew) }
      <button onClick={this.setBeers}>Get beers</button>

      <h2>Favorites</h2>
      { this.state.favorites.map(brew) }
    </div>
  }
}
